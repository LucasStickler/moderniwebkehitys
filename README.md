Moderni web-kehitys

"Moderni web-kehitys" on edistyneempi versio tutkinnon osasta "Sähköisten asiointipalvelujen toteuttaminen" opiskelijoille, joilla on enemmän koodauskokemusta ja jotka ovat jo.osoittaneet osaamisensa ohjelmiston määrittelyssä ja suunnittelussa. Kurssilla käydään läpi web-palvelujen toteuttaminen HTML5-tekniikoilla (HTML5/CSS3/JQuery/PHP) sekä käytetään versionhallintatyökaluja.
Havainto 29.5.

Internet Explorer 11 ei pidä koodaustyylistäni (ei käytännössä mistään moderneista HTML5-, CSS- tai Javascript-tempuista). Testaa portfolioon menevät sivusi IE:ssä. Jos näyttää ihan hassulta, lisää tämä head-osioon: <meta http-equiv="X-UA-Compatible" content="IE=edge"> Sen pitäisi korjata ongelma.

Tutkinnon osan suorittaminen ja portfolio

Lue tämä ensin. Tässä toteutustavassa ei ole erillistä näyttöprojektia. Sen sijaan jakson aikana kootaan portfolio. Portfolioon lisättävät tehtävät on merkitty tähdillä.

Perusta portfoliotasi varten Bitbucket-repository, johon kutsut myös opettajan (paula.kuosmanen@omnia.fi).

Tee repositoryyn myös wiki, joka sisältää tehtäväkuvaukset jokaisen omalla sivullaan (linkit kuvauksiin ja tehtävien kansioihin etusivulta).

Suositeltava kansiorakenne on:
- HTML ja CSS
  - Harjoitus 1
  - Harjoitus 2
  - jne.
- LESS
- Javascript
- jQuery
- PHP
- AJAX